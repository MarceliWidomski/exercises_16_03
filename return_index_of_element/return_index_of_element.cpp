#include <iostream>
using namespace std;

int main() {
	cout << "Program zwraca indeks szukanego elementu " << endl;
	int maxSize(1000);
	int t[maxSize] = { };
	int size(1001);
	int *pt = t;
	while (size > maxSize) {
		cout << "Podaj rozmiar tablicy: ";
		cin >> size;
		if (size > maxSize)
			cout << "Przekroczony maksymalny rozmiar tablicy. ";
	}
	for (int i = 0; i < size; ++i) {
		t[i] = i;
	}
	cout << "Podaj wartosc elementu ktory chcialbys znalezc: ";
	int elementValue;
	cin >> elementValue;
	bool exists(0);
	for (int i = 0; i < size; ++i) {
		if (*pt == elementValue) {
			exists = 1;
			cout << " i = " << *pt << endl;
			cout << "Wartosc znajduje sie pod indeksem " << i << endl;
			break;
		}
		++pt;
	}
	if (exists == 0)
		cout << "Szukana wartosc nie isnieje." << endl;
	return 0;
}
