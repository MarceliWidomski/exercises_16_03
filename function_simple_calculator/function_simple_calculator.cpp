#include <iostream>
using namespace std;

void calculator(); // function declaration

int main() {
	calculator();
	return 0;
}

void calculator() { // function definition
	cout << "Simple calculator" << endl;
	cout << "Enter operation sigh (+, -, *, /): ";
	char sign;
	cin >> sign;
	cout << "Enter first number: ";
	int a;
	cin >> a;
	cout << "Enter second number: ";
	int b;
	cin >> b;
	int result(0);
	switch (sign) {
	case '+':
		result = a + b;
		break;
	case '-':
		result = a - b;
		break;
	case '*':
		result = a * b;
		break;
	case '/': {
		if (b == 0){
			cout << "Dividing by 0!!!" << endl;
			return;
		}
		else
		result = a / b;
	}
		break;
	default:
		cout << "Unknown sign" << endl;
	}
	cout << a << " " << sign << " " << b << " = " << result << endl;
	return;
}
