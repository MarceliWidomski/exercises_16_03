#include <iostream>
using namespace std;

int main() {
	cout
			<< "Program pobiera okreslona ilosc liczb do tablicy i je wypisuje. Wprowadzenie cyfry 0 przerywa wpisywanie liczb."
			<< endl;
	int const maxSize(100);
	int t[maxSize]={0}; // array declaration
	int *it =t;
	int temp(0);
	for (int i = 0; i < maxSize; ++i) { // fills array
		cout << "Wprowadz " << i << " element talicy: ";
		cin >> temp;
		if (temp == 0)
			break;
		*it = temp;
		it++;
	}
	it=t;
	for (int i = 0; i < maxSize; ++i) { // writing out array elements
		cout << *it << " ";
		it++;
		if (*it == 0)
			break;
	}
	return 0;
}
