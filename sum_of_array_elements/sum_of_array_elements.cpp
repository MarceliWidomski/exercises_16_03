//============================================================================
// Name        : sum_of_array_elements.cpp
// Author      :
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

int main() {
	cout << "Program wypisuje sume elementow tablicy" << endl;
	int maxSize(1000);
	int t[maxSize]={0};
	int size(1001);
	int *it=t;
	while (size > maxSize) {
		cout << "Podaj ilosc elementow tablicy " << endl;
		cin >> size;
		if (size > maxSize)
			cout
					<< "Podany rozmiar tablicy jest zbyt duzy. Maksymalny rozmiar tablicy wynosi 1000. "
					<< endl;
	}
	cout << "Wpisz kolejne elementy tablicy: " << endl;
	for (int i = 0; i < size; ++i) {
		cin >> *it++;
	}
	it=t;
	int sum(0);
	for (int i = 0; i < size; ++i) {
		sum+=*it++;
		}
		cout << "Suma elementow tablicy wynosi: " << sum << endl;
	return 0;
}
