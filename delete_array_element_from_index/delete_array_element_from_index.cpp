#include <iostream>
using namespace std;

int main() {
	cout << "Program usuwa element z tablicy z podanej pozycji" << endl;
	int maxSize(1000);
	int t[maxSize] = {0};
	int size(1001);
	int elementNumber(0);
	int *it = &t[elementNumber];
	while (size > maxSize) {
		cout << "Podaj rozmiar tablicy: ";
		cin >> size;
		if (size > maxSize)
			cout << "Przekroczony maksymalny rozmiar tablicy. ";
	}
	for (int i = 0; i < size; ++i) {
		*it = i;
		++it;
	}
	int option(1);
	while (option == 1) {
		cout << "Podaj numer elementu ktory chcialbys usunac: ";
		cin >> elementNumber;
		it = &t[elementNumber];
		int *jt = it + 1;
		int*isize = &t[size - 1];
		if (elementNumber < 0 || elementNumber > size - 1)
			cout << "Element o podanym numerze nie istnieje " << endl;
		else {
			cout << "Usunieto element." << endl;
			for (int i = elementNumber; i < size - 1; ++i) {
				*it = *jt;
				++it;
				++jt;
			}
			*isize = 0;
			--isize;
			--size;
		}
		option = 0;
		while (option != 1 && option != 2) {
			cout << "Czy chcesz usunac kolejny element? " << endl;
			cout << "1) Tak" << endl;
			cout << "2) Nie" << endl;
			cin >> option;
			if (option != 1 && option != 2)
				cout << "Niewlasciwa opcja. Sprobuj ponownie. " << endl;
		}
	}
	cout << "Aktualna tablica to: " << endl;
	it = t;
	for (int i = 0; i < size; ++i)
		cout << *it++ << " ";
	return 0;
}
