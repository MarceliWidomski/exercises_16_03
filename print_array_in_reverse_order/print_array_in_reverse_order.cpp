
#include <iostream>
using namespace std;

int main() {
	cout
			<< "Program pobiera okreslona ilosc liczb do tablicy i je w odwrotnej kolejnosci. Wprowadzenie cyfry 0 przerywa wpisywanie liczb."
			<< endl;
	int const maxSize(100);
	int t[maxSize]={0}; // array declaration
	int *it =t;
	for (int i= 0; i < maxSize; ++i) { // enters array elements
		cout << "Wprowadz " << i << " element talicy: ";
		cin >> *it;
		if (*it == 0)
			break;
		++it;
	}
	for (int i = 0; i < maxSize; ++i) { // writing out array elements
		if (it==t)
			break;
		//it--;
		cout << *(--it) << " ";
	}

	return 0;
}
