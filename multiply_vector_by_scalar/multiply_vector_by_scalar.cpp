#include <iostream>
using namespace std;

int main() {
	int maxSize(1000);
	int vector[maxSize];
	int *iVec = vector;
	cout
			<< "Program multiplies vector by a scalar ( number of vector dimentions is smaller or equal to 1000)"
			<< endl;
	cout << "Enter number of vector dimentions: ";
	int dimentions;
	cin >> dimentions;
	cout << "Enter vector name: ";
	char vectorName;
	cin >> vectorName;
	for (int i = 0; i < dimentions; ++i) {
		cout << "Enter " << i + 1 << " coordinate of vector: " << vectorName
				<< ": ";
		cin >> *iVec;
		++iVec;
	}
	iVec = vector;
	cout << "Emter scalar: ";
	int factor;
	cin >> factor;
	int multiplied[maxSize] = { };
	int *iMultiplied = multiplied;
	for (int i = 0; i < dimentions; ++i) {
		*iMultiplied = *iVec * factor;
		++iMultiplied;
		++iVec;
	}
	iMultiplied=multiplied;
	cout << "Vector " << vectorName << " times " << factor << " equals vector"
			<< endl;
	for (int i = 0; i < dimentions; ++i) {
		cout << *iMultiplied << " ";
		++iMultiplied;
	}
	return 0;
}
