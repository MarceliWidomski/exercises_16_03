#include <iostream>
using namespace std;

int main() {
	cout
			<< "Program zlicza ilosc wystapien liczby 43 w tablicy. Wprowadzenie cyfry 0 przerywa wpisywanie liczb."
			<< endl;
	int const maxSize(100);
	int t[maxSize]={0}; // array declaration
	int *it = t;
	for (int i = 0; i < maxSize; ++i) { // enters array elements
		cout << "Wprowadz " << i << " element talicy: ";
		cin >> *it;
		if (*it == 0)
			break;
		++it;
	}
	it=t;
	int counter(0);
	for (int i = 1; i < maxSize; ++i) { // writes out array elements
		if (*it++ == 43) {
			++counter;
		}
	}
	cout << "Liczba 43 wystepuje w tablicy " << counter << " razy." << endl;
	return 0;
}
