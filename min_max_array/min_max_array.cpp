#include <iostream>
using namespace std;

int main() {
	cout
			<< "Program pobiera okreslona ilosc liczb do tablicy i zwraca min i max. Wprowadzenie cyfry 0 przerywa wpisywanie liczb."
			<< endl;
	int const maxSize(100);
	int t[maxSize] = { 0 }; // array declaration
	int temp(0);
	int *it = t;
	for (int i = 0; i < maxSize; ++i) { // enters array elements
		cout << "Wprowadz " << i << " element talicy: ";
		cin >> temp;
		if (temp == 0)
			break;
		*it++ = temp;
	}
	int min(*it);
	int max(*it);
	it = t;
	for (int i = 1; i < maxSize; ++i) { // writes out array elements
		if (*it > max) // sets max and min value
			max = *it;
		else if (*it < min)
			min = *it;
		++it;
	}
	cout << "Najmniejsza wartosc: " << min << endl;
	cout << "Najwieksza liczba: " << max << endl;
	return 0;
}
