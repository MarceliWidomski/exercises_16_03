#include <iostream>
using namespace std;

int add (int a, int b); // declaration

int main() {
	cout << "Program adds two numbers" << endl;
cout << "Enter first number: ";
int x;
cin >> x;
cout << "Enter second number: ";
int y;
cin >> y;
int result = add(x,y);
cout << "Result: " << result << endl;
	return 0;
}

int add (int a, int b){ // definition
	return a+b;
}
